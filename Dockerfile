FROM registry.gitlab.com/asparanc/ynov_devops/alpine-php-apache:latest

ADD index.php /var/www/localhost/htdocs
ADD src /var/www/localhost/htdocs/src
ADD vendor /var/www/localhost/htdocs/vendor
RUN rm /var/www/localhost/htdocs/index.html

EXPOSE 80

CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]